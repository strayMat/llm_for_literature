
== Relevant presentations

=== Conversational search, Laure Soulier (MLIA, Inria)

Particular challenges of conversational search compared to Info retrieval. 

- Generate in NLP, contextualize ranking with conversation

- Elicite user's information through clarification, identify session drift 

- Evaluation is hard

Use of LLM: T5 and Llama.

Task: 

1) Contextualizing ranking documents with conversation. Only test dataset with canonical answers for each turn. Training is only rewriting of questions with an answer. The pipeline is to leverage sparse (efficient) ranking models for a first round low precision query (SPLADE BM25 model). Then second-stage ranking (MonoT5).  


2) Generating relevant NL answers for complex information needs

Incorporate a structure in the answer. They use planning based strategies. First generate the plan of the answer. Then generate the answer. 

3) Teaching language model when to search

Goal: reduce hallucination, solutions:

- Retrieval-augmented generation
- Calling external API (toolformer)

Limitations: maybe the LLM can answer with its parameters, the model use almost always the API without using its interal model.

Their contribution: use a LLM at inference to identify when it answers correctly or make mistake, fine-tune LLM using hallucination masking mechanism allowing to mask wrong answers with <search> tokens.

4) Generating multi-turn clarification sessions

When multi-turn are beneficiary ? 
In a round fashion: 
- Generate a set of clarying questions given the initial topic. 
- Select two clarifying questions according to the user feedback.

They can do better than just ranking the answers with the query. 

Main issue: they don't have dataset to train the models. Their objective is to augment usual dataset used in IR (query, documents, relevance pairs) with **clarifying questions and user answers?** To provide that, they need to augment the query with an intent that match clarifying questions. These intents are used are intermediate tasks when generating answers. 

Take away: reformulation of questions and interactions seems important.  

