#import "template.typ": *

// Take a look at the file `template.typ` in the file panel
// to customize this template and discover how it works.
#show: project.with(
  title: "Modèle génératifs de langage pour l'aide à la revue de littérature",
  authors: (
    (name: "Matthieu Doutreligne", email: "m.doutreligne@has-sante.fr", affiliation: "HAS"),
  ),
  abstract: "",
  date: datetime.today().display("[day]:[month]:[year]"),
)
#show link: underline

#show raw: box.with(
  fill: luma(240),
  inset: (x: 3pt, y: 0pt),
  outset: (y: 3pt),
  radius: 2pt,
)
//#let citep(content) = cite(brackets: true, content: content)
//#set cite(brackets: false)
// We generated the example code below so you can see how
// your document will look. Go ahead and replace it with
// your own content!

= Introduction

Systematic Reviews (SR) of the literature play an important role in the HAS production processes.

We need to better understand where and how LLMs could be usefull for literature reviews. 

A systematic review is made of several steps @higginscochrane:
- defining the research question and the scope of the review
- defining the inclusion/exclusion criteria
- defining the search strategy
- running the search
- screening the results
- extracting the data
- synthesizing the results

The opportunities for automation and tooling seems particularly important for the screening step.

= Review for the internship

== Studies focused on the screening step

@chappell2023machine review the different machine learning screening tools showcasing overall good sensitivity (close to 1) and a very heterogeneous specificity from 0.2 to 0.9.
 

@gargari2023enhancing assess the capabilities of GPT-3.5 for study eligibility thanks to title and abstract screening. They used as a data source a recently published SR titled 'Light Therapy in Insomnia Disorder: A Systematic Review and
Meta-Analysis'. They ran the original query documented in the SR to retrieve all documents of the SR and assess if GPT was capable of retaining only the final document of the SR. All #link("https://github.com/mamishere/Article-Relevancy-Extraction-GPT3.5-Turbo/")[code, prompts and data are available on github]. The performances are quite poor:

#image("img/gargari2023_results.png", width: 100%)

@li2023sensitivity assess the capabilities of three text mining tools on a retrospective SR of 500 citations, taking the selected paper as a gold standard. They compare Conventional Double screen (CDS), single screen (SS) and double screen with TM (DSTM), concluding in high agreement between CDS and DSTM (around 97%, 95% for sensitivity/specificity for both) and 216 minutes saved with TM. 

== Studies on automating other steps of the SR

Several works studied different steps of the automation process of SRs. However, all of them focus on one systematic review with dedicated labelling and do not publish their data. There is a need to design a dataset with several reviews for robust and detailed analys of the capabilities of automation. Furthermore, only few articles focus on the use of large language models for automation. Finally, we did not found any open-source tool in which we could plug in  standard bibliography management formats (such as rst or bibtex). It is noticable to see that most of the studies do not publish any code.

@Brassey2019Developing developped a fully automated evidence synthesis system for intervention studies. They based their work on the #link("https://www.tripdatabase.com/")[Trip database]. They measured on 25 abstarcts the PICO with a inter-annotator average accuracy of 0.70 for the population, 0.66 for the intervention and 0.62 for the comparator. The ML model performances trained/tested on 1400/300 abstracts was a precision, respectively recall of 0.89/0.87 for the population, 0.72/0.88 for the intervention and 0.87/0.87 for the comparison. Accuracy for sample size extraction was 80% (N=241 RCT abstracts). The model is not detailed. This is pre-LMM.

@Wang2023Can studies the formulation of boolean queries with ChaGPT from natural language.

@thomas2021machine studied machine learning for classification of studies as
RCTs.  They conclude with a high recall (0.99) and a low precision (0.08). 


@jardim2022automating tested the deployment of RobotReviewer for Risk of Bias Assessment in a real SR at the Norwegian Institute of Public Health (NIPH). Robot Reviewer was previously deployed by @marshall2016robotreviewer and exceeded the accuracy of human reviewers by 1-2% in @zhang2016rationale. The authors report similar accuracy to human reviewers, but unequal adoption. They did not assess time gains. 

@muller2023effect published a protocol to study in-depth the capabilities of Machine Learning models for automating SR at the NIPH. It is an excellent source to start reviewing the literature on the automation of SR.

== Protocols helping how to evaluate a task on literature reviews


#pagebreak()
#bibliography("references.bib", style: "chicago-author-date")
